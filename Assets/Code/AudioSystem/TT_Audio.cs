﻿using System;
using UnityEngine;

[Serializable]
public class TT_Audio
{
    [Header("Main Settings")]
    public TT_AudioType AudioType = null;
    public AudioClip AudioClip = null;
    [Range(0f, 1f)] public float Volume = 1f;
    public bool IsOneShot = false;
    public bool IsLooped = false;

    [Header("Owner")]
    public Transform Parent = null;
    public Vector3 LocalPosition = Vector3.zero;

    [Header("Spatial Blend Settings")]
    [Range(0f, 1f)] public float SpatialBlend = 0f;
    public AudioRolloffMode VolumeRolloff = AudioRolloffMode.Logarithmic;
    public float MinDistance = 1f;
    public float MaxDistance = 500f;

    [HideInInspector] public AudioSource AudioSource = null;
    [HideInInspector] public Coroutine PlayCoroutine = null;
}
