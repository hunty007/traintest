﻿using System;
using System.Collections;
using UnityEngine;

public class TT_AudioSystem : MonoBehaviour
{
    [SerializeField] TT_AudioObjectsPool audioObjectsPool = null;

    private static TT_AudioSystem instance = null;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public static void Play(TT_Audio _audio)
    {
        if(instance == null || _audio == null)
        {
            return;
        }

        if(_audio.IsOneShot == false && _audio.AudioSource != null && _audio.AudioSource.isPlaying == true)
        {
            return;
        }

        AudioSource _audioSource = instance.audioObjectsPool?.Get();
        if(_audioSource == null)
        {
            return;
        }

        _audio.AudioSource = _audioSource;

        _audioSource.clip = _audio.AudioClip;
        _audioSource.volume = _audio.Volume;
        _audioSource.loop = _audio.IsLooped;

        _audioSource.transform.parent = _audio.Parent;
        _audioSource.transform.localPosition = _audio.LocalPosition;

        _audioSource.spatialBlend = _audio.SpatialBlend;
        _audioSource.rolloffMode = _audio.VolumeRolloff;
        _audioSource.minDistance = _audio.MinDistance;
        _audioSource.maxDistance = _audio.MaxDistance;

        _audioSource.Play();

        Coroutine _playCoroutine = instance.StartCoroutine(playAudio(_audioSource, _audio));
        if(_audio.IsOneShot == false)
        {
            _audio.PlayCoroutine = _playCoroutine;
        }
    }

    public static void Stop(TT_Audio _audio)
    {
        if (instance == null)
        {
            return;
        }

        AudioSource _audioSource = _audio.AudioSource;
        if (_audioSource == null || _audioSource.isPlaying == false)
        {
            return;
        }

        if (_audio.PlayCoroutine != null)
        {
            instance.StopCoroutine(_audio.PlayCoroutine);
        }

        _audioSource.Stop();
        instance.audioObjectsPool.Release(_audioSource);
    }

    private static IEnumerator playAudio(AudioSource _audioSource, TT_Audio _audio)
    {
        yield return new WaitWhile(() => _audioSource.isPlaying);

        _audio.AudioSource = null;
        instance.audioObjectsPool.Release(_audioSource);
    }
}
