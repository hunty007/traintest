﻿using System.Collections.Generic;
using UnityEngine;

public class TT_AudioManager : MonoBehaviour
{
    [SerializeField] List<TT_Audio> audios = new List<TT_Audio>();

    public void Play(TT_AudioType _audioType)
    {
        TT_Audio _audio = getAudio(_audioType);
        if (_audio == null)
        {
            return;
        }

        TT_AudioSystem.Play(_audio);
    }

    public void Stop(TT_AudioType _audioType)
    {
        TT_Audio _audio = getAudio(_audioType);
        if (_audio == null)
        {
            return;
        }

        TT_AudioSystem.Stop(_audio);
    }

    private TT_Audio getAudio(TT_AudioType _audioType)
    {
        for (int i = 0; i < audios.Count; i++)
        {
            if(audios[i].AudioType == _audioType)
            {
                return audios[i];
            }
        }

        return null;
    }
}
