﻿using UnityEngine;

public class TT_PathFollower : MonoBehaviour
{
    TT_Curve path = null;
    float progress = 0f;
    float progressOffset = 0f;

    public float FullProgress => progress + progressOffset;

    Transform myTransform = null;

    private void Awake()
    {
        myTransform = transform;
    }

    public void SetPath(TT_Curve _path)
    {
        path = _path;
    }

    public void SetProgress(float _progress)
    {
        progress = _progress;
        refreshState();
    }

    public void SetProgressOffset(float _progressOffset)
    {
        progressOffset = _progressOffset;
        refreshState();
    }

    private void refreshState()
    {
        if (path == null)
        {
            return;
        }

        Vector3 _position = path.GetPosition(FullProgress);
        myTransform.position = _position;

        Vector3 _direction = path.GetDirection(FullProgress);
        myTransform.LookAt(_position + _direction);
    }

    public void SetStateWithFullData(TT_Curve _path, float _progress, float _progressOffset)
    {
        if (_path == null)
        {
            return;
        }

        float _fullProgress = _progress + _progressOffset;

        Vector3 _position = _path.GetPosition(_fullProgress);
        transform.position = _position;

        Vector3 _direction = _path.GetDirection(_fullProgress);
        transform.LookAt(_position + _direction);
    }
}
