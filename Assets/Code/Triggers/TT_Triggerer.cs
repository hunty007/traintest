﻿using UnityEngine;

public class TT_Triggerer : MonoBehaviour
{
    private void OnTriggerEnter(Collider _other)
    {
        TT_ITriggerEnter _iTriggerEnter = _other.GetComponent<TT_ITriggerEnter>();
        _iTriggerEnter?.TriggerEnter(gameObject);
    }

    private void OnTriggerExit(Collider _other)
    {
        TT_ITriggerExit _iTriggerExit = _other.GetComponent<TT_ITriggerExit>();
        _iTriggerExit?.TriggerExit(gameObject);
    }
}
