﻿using UnityEngine;

public interface TT_ITriggerEnter
{
    void TriggerEnter(GameObject _caller);
}
