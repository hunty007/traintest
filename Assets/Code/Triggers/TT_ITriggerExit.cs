﻿using UnityEngine;

public interface TT_ITriggerExit
{
    void TriggerExit(GameObject _caller);
}
