﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TT_Train : MonoBehaviour
{
    [Serializable]
    public struct TrainElementHolder
    {
        public TT_PathFollower PathFollower;
        public float ProgressOffset;
    }

    public enum EMoveType
    {
        Back = 0,
        Loop = 1
    }

    [Header("Movement")]
    [SerializeField] float moveSpeed = 1f;
    [SerializeField] EMoveType moveType = EMoveType.Back;

    TT_TrainSystem trainSystem = null;
    TT_Curve path = null;
    float progress = 0f;
    float progressOffset = 0f;
    float moveDirection = 1f;

    public float FullProgress => progress + progressOffset;

    [Header("Elements")]
    [SerializeField] List<TrainElementHolder> trainElementHolders = new List<TrainElementHolder>();

    [Header("Wagons")]
    [SerializeField] float openWagonDoorsForceTime = 5f;

    List<TT_Wagon> wagons = new List<TT_Wagon>();
    WaitForSeconds openWagonDoorsForceTimeWaiter = null;
    Coroutine openWagonsDoorsCoroutine = null;

    [Header("Stop")]
    bool isStopped = false;
    bool lastIsStopped = false;
    bool didUseStopButton = false;

    public bool IsStopped
    {
        get => isStopped;
        set
        {
            lastIsStopped = isStopped;
            isStopped = value;
        }
    }

    [Header("Audio")]
    [SerializeField] TT_AudioType moveAudioType = null;
    [SerializeField] TT_AudioType stopAudioType = null;

    List<TT_AudioManager> myAudioManagers = new List<TT_AudioManager>();

    private void Awake()
    {
        GetComponentsInChildren(myAudioManagers);

        GetComponentsInChildren(wagons);
        for (int i = 0; i < wagons.Count; i++)
        {
            wagons[i].RegisterStopButtonAction(onStopButtonInteract);

            wagons[i].RegisterDoorsOpenAction(onWagonDoorOpenClose);
            wagons[i].RegisterDoorsCloseAction(onWagonDoorOpenClose);
        }

        openWagonDoorsForceTimeWaiter = new WaitForSeconds(openWagonDoorsForceTime);
    }

    private void Start()
    {
        refreshState();
    }

    public void SetTrainSystem(TT_TrainSystem _trainSystem)
    {
        trainSystem = _trainSystem;
    }

    public bool Move(out float _previousProgress, out float _currentProgress)
    {
        if (path == null)
        {
            _previousProgress = FullProgress;
            _currentProgress = FullProgress;
            return false;
        }

        if(isStopped)
        {
            refreshState();

            toggleMoveSound(false);
            if (lastIsStopped == false)
            {
                playStopSound();
            }

            _previousProgress = FullProgress;
            _currentProgress = FullProgress;
            return false;
        }

        _previousProgress = FullProgress;
        progress += moveDirection * moveSpeed * Time.deltaTime;
        if (FullProgress > 1f)
        {
            switch (moveType)
            {
                case EMoveType.Loop:
                    progress -= 1f;
                    break;

                case EMoveType.Back:
                    progress = (1f - progressOffset) - (progress - (1f - progressOffset));
                    moveDirection *= -1f;
                    break;
            }
        }
        else if(FullProgress < 0f)
        {
            switch (moveType)
            {
                case EMoveType.Loop:
                    progress += 1f;
                    break;

                case EMoveType.Back:
                    progress -= progress;
                    moveDirection *= -1f;
                    break;
            }
        }

        refreshState();

        toggleMoveSound(true);

        _currentProgress = FullProgress;
        return true;
    }

    public void StopByStation()
    {
        IsStopped = true;
        openDoorsOnArrived();
    }

    public void SetPath(TT_Curve _path)
    {
        path = _path;

        for (int i = 0; i < trainElementHolders.Count; i++)
        {
            trainElementHolders[i].PathFollower.SetPath(path);
        }
    }

    public void SetProgress(float _progress)
    {
        progress = _progress;
        refreshState();
    }

    public void SetProgressOffset(float _progressOffset)
    {
        progressOffset = _progressOffset;
        refreshState();
    }

    private void refreshState()
    {
        for (int i = 0; i < trainElementHolders.Count; i++)
        {
            TrainElementHolder _trainElementHolder = trainElementHolders[i];
            TT_PathFollower _pathFollower = _trainElementHolder.PathFollower;

            _pathFollower.SetProgress(FullProgress);
            _pathFollower.SetProgressOffset(_trainElementHolder.ProgressOffset);
        }
    }

    private void toggleMoveSound(bool _isActive)
    {
        if (_isActive)
        {
            for (int i = 0; i < myAudioManagers.Count; i++)
            {
                myAudioManagers[i]?.Play(moveAudioType);
            }
        }
        else
        {
            for (int i = 0; i < myAudioManagers.Count; i++)
            {
                myAudioManagers[i]?.Stop(moveAudioType);
            }
        }
    }

    private void playStopSound()
    {
        for(int i = 0; i < myAudioManagers.Count; i++)
            {
            myAudioManagers[i]?.Play(stopAudioType);
        }
    }

    private void onStopButtonInteract()
    {
        if(didUseStopButton)
        {
            return;
        }

        didUseStopButton = true;
        trainSystem.OnTrainArrivedTrainStation += stopTrainFromStopButton;
    }

    private void stopTrainFromStopButton(TT_Train _train, TT_TrainStation _trainStation)
    {
        if (_train != this)
        {
            return;
        }

        didUseStopButton = false;
        IsStopped = true;
        openDoorsOnArrived();

        trainSystem.OnTrainArrivedTrainStation -= stopTrainFromStopButton;
    }

    public void SetStateWithFullData(TT_Curve _path, float _progress)
    {
        for (int i = 0; i < trainElementHolders.Count; i++)
        {
            TrainElementHolder _trainElementHolder = trainElementHolders[i];
            TT_PathFollower _pathFollower = _trainElementHolder.PathFollower;

            _pathFollower.SetStateWithFullData(_path, _progress, _trainElementHolder.ProgressOffset);
        }
    }

    private IEnumerator openWagonsDoorsWithForce()
    {
        for (int i = 0; i < wagons.Count; i++)
        {
            wagons[i].SetDoorsOpenForce(true);
        }

        yield return openWagonDoorsForceTimeWaiter;

        for (int i = 0; i < wagons.Count; i++)
        {
            wagons[i].SetDoorsOpenForce(false);
        }
    }

    private void openDoorsOnArrived()
    {
        for (int i = 0; i < wagons.Count; i++)
        {
            wagons[i].SetDoorsLock(false);
        }

        openWagonsDoorsCoroutine = StartCoroutine(openWagonsDoorsWithForce());
    }

    private void onWagonDoorOpenClose()
    {
        bool _isStopped = false;
        for (int i = 0; i < wagons.Count; i++)
        {
            if (wagons[i].IsAnyDoorOpened())
            {
                _isStopped = true;
                break;
            }
        }

        IsStopped = _isStopped;

        if (isStopped == false)
        {
            for (int i = 0; i < wagons.Count; i++)
            {
                wagons[i].SetDoorsLock(true);
            }

            if (openWagonsDoorsCoroutine != null)
            {
                StopCoroutine(openWagonsDoorsCoroutine);
            }
            for (int i = 0; i < wagons.Count; i++)
            {
                wagons[i].SetDoorsOpenForce(false);
            }
        }
    }
}
