﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TT_WagonDoor : MonoBehaviour
{
    public event Action OnDoorOpened = null;
    public event Action OnDoorClosed = null;

    private readonly int IS_OPENED_ID = Animator.StringToHash("IsOpened");

    [SerializeField] Animator doorAnimator = null;

    TT_Wagon wagon = null;
    List<GameObject> triggerers = new List<GameObject>();
    bool isOpened = false;

    public bool IsOpened => isOpened;

    private void Awake()
    {
        wagon = GetComponentInParent<TT_Wagon>();
    }

    public void AddTriggerer(GameObject _triggerer)
    {
        triggerers.Add(_triggerer);
        RefreshDoorState();
    }

    public void RemoveTriggerer(GameObject _triggerer)
    {
        triggerers.Remove(_triggerer);
        RefreshDoorState();
    }

    public void OpenFromAnimation()
    {
        isOpened = true;
        OnDoorOpened?.Invoke();
    }

    public void CloseFromAnimation()
    {
        isOpened = false;
        OnDoorClosed?.Invoke();
    }

    public void RefreshDoorState()
    {
        if (wagon.ShouldForceDoorsToOpen
            || (wagon.CanDoorsBeOpened == false && triggerers.Count > 0))
        {
            doorAnimator.SetBool(IS_OPENED_ID, true);
        }
        else
        {
            doorAnimator.SetBool(IS_OPENED_ID, false);
        }
    }
}
