﻿using UnityEngine;

public class TT_TrainStation : MonoBehaviour
{
    [Header("Call button")]
    [SerializeField] TT_Button trainCallButton = null;

    bool didUse = false;

    TT_TrainSystem trainSystem = null;

    private void Awake()
    {
        trainCallButton.OnInteracted += summonTrain;
    }

    public void SetTrainSystem(TT_TrainSystem _trainSystem)
    {
        trainSystem = _trainSystem;
    }

    private void summonTrain()
    {
        if (trainSystem == null || didUse)
        {
            return;
        }

        didUse = true;
        trainSystem.OnTrainArrivedTrainStation += onTrainArrivedTrainStation;
    }

    private void onTrainArrivedTrainStation(TT_Train _train, TT_TrainStation _trainStation)
    {
        if (_trainStation != this)
        {
            return;
        }

        didUse = false;
        _train.StopByStation();

        trainSystem.OnTrainArrivedTrainStation -= onTrainArrivedTrainStation;
    }
}
