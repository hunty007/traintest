﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TT_TrainSystem : MonoBehaviour
{
    [Serializable]
    public struct TrainHolder
    {
        public TT_Train Train;
        [Range(0f, 1f)] public float Progress;
        [Range(0f, 1f)] public float ProgressOffset;
    }

    [Serializable]
    public struct TrainStationHolder
    {
        public TT_TrainStation TrainStation;
        [Range(0f, 1f)] public float Progress;
        public Vector3 PositionOffset;
        public Vector3 RotationOffset;
    }

    [Header("Path")]
    [SerializeField] TT_Curve path = null;

    [Header("Train")]
    [SerializeField] TT_Train trainPrefab = null;
    [SerializeField] List<TrainHolder> trainHolders = new List<TrainHolder>();

    [Header("Train Station")]
    [SerializeField] TT_TrainStation trainStationPrefab = null;
    [SerializeField] List<TrainStationHolder> trainStationHolders = new List<TrainStationHolder>();

    public event Action<TT_Train, TT_TrainStation> OnTrainArrivedTrainStation = null;

    private void Awake()
    {
        for (int i = 0; i < trainHolders.Count; i++)
        {
            TrainHolder _trainHolder = trainHolders[i];
            _trainHolder.Train.SetTrainSystem(this);
            _trainHolder.Train.SetPath(path);
            _trainHolder.Train.SetProgress(_trainHolder.Progress);
            _trainHolder.Train.SetProgressOffset(_trainHolder.ProgressOffset);
        }

        for (int i = 0; i < trainStationHolders.Count; i++)
        {
            TrainStationHolder _trainStationHolder = trainStationHolders[i];
            _trainStationHolder.TrainStation.SetTrainSystem(this);
        }
    }

    private void Update()
    {
        for (int i = 0; i < trainHolders.Count; i++)
        {
            TT_Train _train = trainHolders[i].Train;

            float _previousProgress = 0f;
            float _currentProgress = 0f;
            bool _didMove = _train.Move(out _previousProgress, out _currentProgress);

            if(_didMove)
            {
                checkTrainArriveToStation(_train, _previousProgress, _currentProgress);
            }
        }
    }

    private void checkTrainArriveToStation(TT_Train _train, float _previousProgress, float _currentProgress)
    {
        for (int i = 0; i < trainStationHolders.Count; i++)
        {
            TT_TrainStation _trainStation = trainStationHolders[i].TrainStation;

            float _progress = trainStationHolders[i].Progress;
            if ((_previousProgress < _progress && _currentProgress >= _progress)
                || (_previousProgress > _progress && _currentProgress <= _progress))
            {
                OnTrainArrivedTrainStation?.Invoke(_train, _trainStation);
            }
        }
    }
}
