﻿using System;
using UnityEngine;

public class TT_TrainWagon : MonoBehaviour,
    TT_ICanInteract, TT_IInteract
{
    [Serializable]
    class Slot
    {
        public Transform Spawner = null;
        public GameObject Person = null;
    }

    [SerializeField] Slot[] slots = new Slot[0];

    TT_Train train = null;

    public bool CanInteract(GameObject _caller)
    {
        int _emptySlotsCount = 0;
        for (int i = 0; i < slots.Length; i++)
        {
            if(slots[i].Person == null)
            {
                _emptySlotsCount++;
            }
            else if (slots[i].Person == _caller)
            {
                return false;
            }
        }

        if(_emptySlotsCount > 0)
        {
            return true;
        }

        return false;
    }

    public void Interact(GameObject _caller)
    {
        Debug.Log($"TT_TrainWagon :: Entered by {_caller}");

        for (int i = 0; i < slots.Length; i++)
        {
            if(slots[i].Person != null)
            {
                continue;
            }

            _caller.transform.position = slots[i].Spawner.position;
            slots[i].Person = _caller;
        }
    }

    public void SetTrain(TT_Train _train)
    {
        train = _train;
    }
}
