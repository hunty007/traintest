﻿using UnityEngine;

public class TT_TrainWagonButton : MonoBehaviour, TT_IInteract
{
    [SerializeField] TT_TrainWagon trainWagon = null;

    public void Interact(GameObject _caller)
    {
        Debug.Log($"TT_TrainWagonButton :: Interact by '{_caller}'");
    }
}
