﻿using UnityEngine;

public class TT_TrainDoor_Photocell : MonoBehaviour, TT_ITriggerEnter, TT_ITriggerExit
{
    TT_WagonDoor trainDoor = null;

    private void Awake()
    {
        trainDoor = GetComponentInParent<TT_WagonDoor>();
    }

    public void TriggerEnter(GameObject _caller)
    {
        trainDoor?.AddTriggerer(_caller);
    }

    public void TriggerExit(GameObject _caller)
    {
        trainDoor?.RemoveTriggerer(_caller);
    }
}
