﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TT_TrainSystem))]
public class TT_TrainSystemEditor : Editor
{
    TT_TrainSystem trainSystem = null;

    SerializedProperty pathProperty = null;
    TT_Curve path = null;

    SerializedProperty trainPrefabProperty = null;
    SerializedProperty trainHoldersProperty = null;

    SerializedProperty trainStationPrefabProperty = null;
    SerializedProperty trainStationHoldersProperty = null;

    private void OnEnable()
    {
        trainSystem = target as TT_TrainSystem;

        pathProperty = serializedObject.FindProperty("path");

        trainPrefabProperty = serializedObject.FindProperty("trainPrefab");
        trainHoldersProperty = serializedObject.FindProperty("trainHolders");

        trainStationPrefabProperty = serializedObject.FindProperty("trainStationPrefab");
        trainStationHoldersProperty = serializedObject.FindProperty("trainStationHolders");
    }

    public override void OnInspectorGUI()
    {
        path = pathProperty.objectReferenceValue as TT_Curve;

        EditorGUILayout.PropertyField(pathProperty);
        EditorGUILayout.Space(10f);
        drawTrains();
        EditorGUILayout.Space(10f);
        drawTrainStations();

        serializedObject.ApplyModifiedProperties();
    }

    private void drawTrains()
    {
        using (new EditorGUILayout.HorizontalScope())
        {
            EditorGUILayout.PropertyField(trainPrefabProperty);
            if (GUILayout.Button("Create", GUILayout.Width(100f)))
            {
                TT_Train _trainPrefab = trainPrefabProperty?.objectReferenceValue as TT_Train;
                if (_trainPrefab == null)
                {
                    Debug.Log("TT_TrainSystem :: No train prefab to instantiate!");
                    return;
                }

                TT_Train _train = (TT_Train)PrefabUtility.InstantiatePrefab(_trainPrefab, trainSystem.transform);

                trainHoldersProperty.InsertArrayElementAtIndex(trainHoldersProperty.arraySize);

                SerializedProperty _trainHolderProperty = trainHoldersProperty.GetArrayElementAtIndex(trainHoldersProperty.arraySize - 1);
                SerializedProperty _trainProperty = _trainHolderProperty.FindPropertyRelative("Train");
                _trainProperty.objectReferenceValue = _train;

                SerializedProperty _progressProperty = _trainHolderProperty.FindPropertyRelative("Progress");
                float _progress = _progressProperty.floatValue;

                SerializedProperty _progressOffsetProperty = _trainHolderProperty.FindPropertyRelative("ProgressOffset");
                float _progressOffset = _progressOffsetProperty.floatValue;

                setupTrain(_train, _progress, _progressOffset);
            }
        }

        if (trainHoldersProperty.arraySize > 0)
        {
            trainHoldersProperty.isExpanded = EditorGUILayout.Foldout(trainHoldersProperty.isExpanded, $"Trains({trainHoldersProperty.arraySize}):", true);

            if (trainHoldersProperty.isExpanded)
            {
                EditorGUI.indentLevel++;

                for (int i = 0; i < trainHoldersProperty.arraySize; i++)
                {
                    SerializedProperty _trainHolderProperty = trainHoldersProperty.GetArrayElementAtIndex(i);
                    SerializedProperty _trainProperty = _trainHolderProperty.FindPropertyRelative("Train");
                    SerializedProperty _progressProperty = _trainHolderProperty.FindPropertyRelative("Progress");
                    SerializedProperty _progressOffsetProperty = _trainHolderProperty.FindPropertyRelative("ProgressOffset");

                    TT_Train _train = _trainProperty.objectReferenceValue as TT_Train;

                    if (_train == null)
                    {
                        trainHoldersProperty.DeleteArrayElementAtIndex(i);
                        i--;

                        continue;
                    }

                    using (new EditorGUILayout.HorizontalScope())
                    {
                        _trainProperty.isExpanded = EditorGUILayout.Foldout(_trainProperty.isExpanded, $"Train {i + 1}", true);

                        if (GUILayout.Button("Remove", GUILayout.Width(100f)))
                        {
                            DestroyImmediate(_train.gameObject);

                            trainHoldersProperty.DeleteArrayElementAtIndex(i);
                            i--;

                            continue;
                        }
                    }

                    if (_trainProperty.isExpanded)
                    {
                        EditorGUI.indentLevel++;

                        GUI.enabled = false;
                        EditorGUILayout.PropertyField(_trainProperty);
                        GUI.enabled = true;

                        using (var _changeCheckScope = new EditorGUI.ChangeCheckScope())
                        {
                            EditorGUILayout.PropertyField(_progressProperty);
                            EditorGUILayout.PropertyField(_progressOffsetProperty);

                            if(_changeCheckScope.changed)
                            {
                                setupTrain(_train, _progressProperty.floatValue, _progressOffsetProperty.floatValue);
                            }
                        }

                        EditorGUI.indentLevel--;
                    }
                }

                EditorGUI.indentLevel--;
            }
        }
        else
        {
            EditorGUILayout.LabelField("No trains");
        }
    }

    private void drawTrainStations()
    {
        using (new EditorGUILayout.HorizontalScope())
        {
            EditorGUILayout.PropertyField(trainStationPrefabProperty);
            if (GUILayout.Button("Create", GUILayout.Width(100f)))
            {
                TT_TrainStation _trainStationPrefab = trainStationPrefabProperty?.objectReferenceValue as TT_TrainStation;
                if (_trainStationPrefab == null)
                {
                    Debug.Log("TT_TrainSystem :: No train station prefab to instantiate!");
                    return;
                }

                TT_TrainStation _trainStation = (TT_TrainStation)PrefabUtility.InstantiatePrefab(_trainStationPrefab, trainSystem.transform);

                trainStationHoldersProperty.InsertArrayElementAtIndex(trainStationHoldersProperty.arraySize);

                SerializedProperty _trainStationHolderProperty = trainStationHoldersProperty.GetArrayElementAtIndex(trainStationHoldersProperty.arraySize - 1);
                SerializedProperty _trainStationProperty = _trainStationHolderProperty.FindPropertyRelative("TrainStation");
                _trainStationProperty.objectReferenceValue = _trainStation;

                SerializedProperty _progressProperty = _trainStationHolderProperty.FindPropertyRelative("Progress");
                float _progress = _progressProperty.floatValue;

                SerializedProperty _positionOffsetProperty = _trainStationHolderProperty.FindPropertyRelative("PositionOffset");
                Vector3 _positionOffset = _positionOffsetProperty.vector3Value;

                SerializedProperty _rotationOffsetProperty = _trainStationHolderProperty.FindPropertyRelative("RotationOffset");
                Vector3 _rotationOffset = _rotationOffsetProperty.vector3Value;

                setupTrainStation(_trainStation, _progress, _positionOffset, _rotationOffset);
            }
        }

        if (trainStationHoldersProperty.arraySize > 0)
        {
            trainStationHoldersProperty.isExpanded =
                EditorGUILayout.Foldout(trainStationHoldersProperty.isExpanded, $"Train stations({trainStationHoldersProperty.arraySize}):", true);

            if (trainStationHoldersProperty.isExpanded)
            {
                EditorGUI.indentLevel++;

                for (int i = 0; i < trainStationHoldersProperty.arraySize; i++)
                {
                    SerializedProperty _trainStationHolderProperty = trainStationHoldersProperty.GetArrayElementAtIndex(i);

                    SerializedProperty _trainStationProperty = _trainStationHolderProperty.FindPropertyRelative("TrainStation");
                    TT_TrainStation _trainStation = _trainStationProperty.objectReferenceValue as TT_TrainStation;

                    SerializedProperty _progressProperty = _trainStationHolderProperty.FindPropertyRelative("Progress");
                    SerializedProperty _positionOffsetProperty = _trainStationHolderProperty.FindPropertyRelative("PositionOffset");
                    SerializedProperty _rotationOffsetProperty = _trainStationHolderProperty.FindPropertyRelative("RotationOffset");

                    if (_trainStation == null)
                    {
                        trainStationHoldersProperty.DeleteArrayElementAtIndex(i);
                        i--;

                        continue;
                    }

                    using (new EditorGUILayout.HorizontalScope())
                    {
                        _trainStationProperty.isExpanded = EditorGUILayout.Foldout(_trainStationProperty.isExpanded, $"Train station {i + 1}", true);

                        if (GUILayout.Button("Remove", GUILayout.Width(100f)) && _trainStation != null)
                        {
                            DestroyImmediate(_trainStation.gameObject);

                            trainStationHoldersProperty.DeleteArrayElementAtIndex(i);
                            i--;

                            continue;
                        }
                    }

                    if(_trainStationProperty.isExpanded)
                    {
                        EditorGUI.indentLevel++;

                        GUI.enabled = false;
                        EditorGUILayout.PropertyField(_trainStationProperty);
                        GUI.enabled = true;

                        using (var _changeCheckScope = new EditorGUI.ChangeCheckScope())
                        {
                            EditorGUILayout.PropertyField(_progressProperty);
                            EditorGUILayout.PropertyField(_positionOffsetProperty);
                            EditorGUILayout.PropertyField(_rotationOffsetProperty);

                            if (_changeCheckScope.changed)
                            {
                                setupTrainStation(_trainStation, _progressProperty.floatValue, _positionOffsetProperty.vector3Value, _rotationOffsetProperty.vector3Value);
                            }
                        }

                        EditorGUI.indentLevel--;
                    }
                }

                EditorGUI.indentLevel--;
            }
        }
        else
        {
            EditorGUILayout.LabelField("No train stations");
        }
    }

    private void setupTrain(TT_Train _train, float _progress, float _progressOffset)
    {
        _train.SetStateWithFullData(path, _progress + _progressOffset);
    }

    private void setupTrainStation(TT_TrainStation _trainStation, float _progress, Vector3 _positionOffset, Vector3 _rotationOffset)
    {
        if (path == null)
        {
            return;
        }

        Vector3 _position = path.GetPosition(_progress);
        _trainStation.transform.position = _position;

        Vector3 _direction = path.GetDirection(_progress);
        _trainStation.transform.LookAt(_position + _direction);
        _trainStation.transform.Rotate(_rotationOffset);

        _trainStation.transform.position += _trainStation.transform.TransformDirection(_positionOffset);
    }
}
