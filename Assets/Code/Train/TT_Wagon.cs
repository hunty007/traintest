﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TT_Wagon : MonoBehaviour
{
    [SerializeField] TT_Button stopButton = null;

    bool shouldForceDoorsToOpen = false;
    bool canDoorsBeOpened = false;

    List<TT_WagonDoor> doors = new List<TT_WagonDoor>();

    public bool ShouldForceDoorsToOpen => shouldForceDoorsToOpen;
    public bool CanDoorsBeOpened => canDoorsBeOpened;

    private void Awake()
    {
        GetComponentsInChildren(doors);
    }

    public void SetDoorsOpenForce(bool _shouldForceDoorsToOpen)
    {
        shouldForceDoorsToOpen = _shouldForceDoorsToOpen;
        refreshDoorStates();
    }

    public void SetDoorsLock(bool _areDoorsLocked)
    {
        canDoorsBeOpened = _areDoorsLocked;
        refreshDoorStates();
    }

    public bool IsAnyDoorOpened()
    {
        for (int i = 0; i < doors.Count; i++)
        {
            if(doors[i].IsOpened)
            {
                return true;
            }
        }

        return false;
    }

    public void RegisterStopButtonAction(Action _onStopButton)
    {
        stopButton.OnInteracted += _onStopButton;
    }

    public void UnregisterStopButtonAction(Action _onStopButton)
    {
        stopButton.OnInteracted += _onStopButton;
    }

    public void RegisterDoorsOpenAction(Action _onDoorsOpened)
    {
        for (int i = 0; i < doors.Count; i++)
        {
            doors[i].OnDoorOpened += _onDoorsOpened;
        }
    }

    public void RegisterDoorsCloseAction(Action _onDoorsClosed)
    {
        for (int i = 0; i < doors.Count; i++)
        {
            doors[i].OnDoorClosed += _onDoorsClosed;
        }
    }

    private void refreshDoorStates()
    {
        for (int i = 0; i < doors.Count; i++)
        {
            doors[i].RefreshDoorState();
        }
    }
}
