﻿using UnityEngine;

public class TT_WagonDoor_AnimatorListener : MonoBehaviour
{
    TT_WagonDoor wagonDoor = null;

    private void Awake()
    {
        wagonDoor = GetComponentInParent<TT_WagonDoor>();
    }

    public void OpenDoor()
    {
        wagonDoor.OpenFromAnimation();
    }

    public void CloseDoor()
    {
        wagonDoor.CloseFromAnimation();
    }
}
