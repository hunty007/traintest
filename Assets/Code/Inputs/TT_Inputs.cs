﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TT_Inputs
{
    // MOVEMENT
    public static float HorizontalMovement => Input.GetAxis("Horizontal");
    public static float VerticalMovement => Input.GetAxis("Vertical");
    public static bool Jump => Input.GetKeyDown(KeyCode.Space);


    // CAMERA
    public static float HorizontalRotation => Input.GetAxis("Mouse X");
    public static float VerticalRotation => Input.GetAxis("Mouse Y");

    // INTERACTION
    public static bool Interact => Input.GetKeyDown(KeyCode.E);
}
