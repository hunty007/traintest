﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TT_PlayerFollower : MonoBehaviour
{
    [SerializeField] Vector3 followOffset = Vector3.zero;

    Transform myTransform = null;
    TT_PlayerMovement player = null;

    private void Awake()
    {
        myTransform = transform;
        player = FindObjectOfType<TT_PlayerMovement>();
    }

    private void Update()
    {
        followPlayer();
    }

    private void followPlayer()
    {
        if (player == null)
        {
            return;
        }

        myTransform.position = player.CameraFollower.position + followOffset;
        myTransform.rotation = player.CameraFollower.rotation;
    }
}
