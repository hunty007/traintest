﻿using UnityEngine;

public interface TT_ICanInteract
{
    bool CanInteract(GameObject _caller);
}
