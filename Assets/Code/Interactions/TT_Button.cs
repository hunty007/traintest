﻿using System;
using UnityEngine;

public class TT_Button : MonoBehaviour, TT_IInteract
{
    public event Action OnInteracted = null;

    [SerializeField] TT_AudioType interactAudioType = null;

    TT_AudioManager myAudioManager = null;

    private void Awake()
    {
        myAudioManager = GetComponent<TT_AudioManager>();
    }

    public void Interact(GameObject _caller)
    {
        myAudioManager?.Play(interactAudioType);

        OnInteracted?.Invoke();
    }
}
