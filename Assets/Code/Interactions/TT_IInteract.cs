﻿using UnityEngine;

public interface TT_IInteract
{
    void Interact(GameObject _caller);
}
