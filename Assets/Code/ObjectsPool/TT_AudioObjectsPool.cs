﻿using UnityEngine;

[CreateAssetMenu(fileName = "audioObjectsPool_NewAudioObjectsPool", menuName = "TrainTest/Audio Objects Pool")]
public class TT_AudioObjectsPool : TT_ObjectsPool<AudioSource>
{
    
}
