﻿using System.Collections.Generic;
using UnityEngine;

public class TT_ObjectsPool<T> : ScriptableObject where T : Object
{
    [SerializeField] T prefab = null;
    [SerializeField] int initCount = 1;

    bool isInitialized = false;
    Queue<T> pool = new Queue<T>();

    public T Get()
    {
        if (isInitialized == false)
        {
            initialize();
        }

        if (pool.Count > 0)
        {
            T _instance = pool.Dequeue();
            return _instance;
        }
        else
        {
            T _instance = Instantiate(prefab);
            return _instance;
        }
    }

    public void Release(T _instance)
    {
        pool.Enqueue(_instance);
    }

    private void initialize()
    {
        isInitialized = true;

        for (int i = 0; i < initCount; i++)
        {
            T _instance = Instantiate(prefab);
            pool.Enqueue(_instance);
        }
    }
}
