﻿using UnityEngine;

public class TT_PlayerMovement : MonoBehaviour
{
    [Header("Move")]
    [SerializeField] Transform mover = null;
    [SerializeField] float moveSpeed = 1f;

    [Header("Jump")]
    [SerializeField] TT_Feet feet = null;
    [SerializeField] float jumpPower = 1f;

    [Header("Rotate")]
    [SerializeField] Transform verticalRotator = null;
    [SerializeField] Transform horizontalRotator = null;
    [SerializeField] float rotateSpeed = 1f;

    float verticalRotation = 0f;

    [Header("Camera")]
    [SerializeField] Transform cameraFollower = null;

    public Transform CameraFollower => cameraFollower;

    Rigidbody myRigidbody = null;

    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        move();
        rotate();
        jump();
    }

    private void move()
    {
        float _horizontalMovement = TT_Inputs.HorizontalMovement;
        float _verticalMovement = TT_Inputs.VerticalMovement;

        Vector3 _velocityFromInputs = new Vector3(_horizontalMovement, 0f, _verticalMovement);
        Vector3 _worldVelocityFromInputs = mover.TransformVector(_velocityFromInputs);

        if(_worldVelocityFromInputs.magnitude >= 1f)
        {
            Vector3 _normalizedWorldVelocityFromInputs = _worldVelocityFromInputs.normalized;
            Vector3 _velocity = new Vector3(_normalizedWorldVelocityFromInputs.x * moveSpeed * Time.deltaTime, myRigidbody.velocity.y, _normalizedWorldVelocityFromInputs.z * moveSpeed * Time.deltaTime); ;
            myRigidbody.velocity = _velocity;
        }
        else
        {
            Vector3 _velocity = new Vector3(_worldVelocityFromInputs.x * moveSpeed * Time.deltaTime, myRigidbody.velocity.y, _worldVelocityFromInputs.z * moveSpeed * Time.deltaTime);
            myRigidbody.velocity = _velocity;
        }
    }

    private void rotate()
    {
        rotateVertically();
        rotateHorizontally();
    }

    private void jump()
    {
        if(TT_Inputs.Jump && feet.IsGrounded)
        {
            Vector3 _velocity = myRigidbody.velocity;
            _velocity.y = jumpPower;
            myRigidbody.velocity = _velocity;
        }
    }

    private void rotateVertically()
    {
        float _vertical = -TT_Inputs.VerticalRotation * rotateSpeed * Time.deltaTime;
        verticalRotation = Mathf.Clamp(verticalRotation + _vertical, -89f, 89f);

        Vector3 _verticalRotation = verticalRotator.localEulerAngles;
        _verticalRotation.x = verticalRotation;
        verticalRotator.localRotation = Quaternion.Euler(_verticalRotation);
    }

    private void rotateHorizontally()
    {
        float _horizontal = TT_Inputs.HorizontalRotation * rotateSpeed * Time.deltaTime;
        Vector3 _horizontalRotation = new Vector3(0f, _horizontal, 0f);
        horizontalRotator.Rotate(_horizontalRotation);
    }
}
