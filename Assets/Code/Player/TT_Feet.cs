﻿using System.Collections.Generic;
using UnityEngine;

public class TT_Feet : MonoBehaviour
{
    List<Collider> triggeredGrounds = new List<Collider>();

    public bool IsGrounded => triggeredGrounds.Count > 0;

    private void OnTriggerEnter(Collider _other)
    {
        triggeredGrounds.Add(_other);
    }

    private void OnTriggerExit(Collider _other)
    {
        triggeredGrounds.Remove(_other);
    }
}
