﻿using UnityEngine;

public class TT_PlayerInteraction : MonoBehaviour
{
    [SerializeField] Transform origin = null;
    [SerializeField] float maxInteractionDistance = 1f;

    Ray ray = new Ray();
    RaycastHit[] raycastHits = new RaycastHit[5];

    TT_IInteract iInteractable = null;

    private void Update()
    {
        bool _didInteract = interact();
        if (_didInteract == false)
        {
            findInteractable();
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Debug.DrawRay(Camera.main.transform.position, maxInteractionDistance * Camera.main.transform.forward, Color.red);
    }
#endif

    private bool interact()
    {
        if (TT_Inputs.Interact)
        {
            iInteractable?.Interact(gameObject);
            return true;
        }

        return false;
    }

    private void findInteractable()
    {
        iInteractable = null;

        ray.origin = Camera.main.transform.position;
        ray.direction = Camera.main.transform.forward;

        int _raycastHitsCount = Physics.RaycastNonAlloc(ray, raycastHits, maxInteractionDistance);
        for (int i = 0; i < _raycastHitsCount; i++)
        {
            TT_ICanInteract _iCanInteract = raycastHits[i].collider?.GetComponent<TT_ICanInteract>();
            if (_iCanInteract?.CanInteract(gameObject) == false)
            {
                iInteractable = null;
                continue;
            }
            else
            {
                TT_IInteract _iInteractable = raycastHits[i].collider?.GetComponent<TT_IInteract>();
                if(_iInteractable == null)
                {
                    continue;
                }
                iInteractable = _iInteractable;
                break;
            }
        }
    }
}
