﻿using UnityEngine;

public class TT_Traveler : MonoBehaviour
{
    [SerializeField] Transform toTransport = null;

    Transform defaultParent = null;

    private void Awake()
    {
        defaultParent = toTransport.parent;
    }

    public void StartTransport(TT_Transporter _transporter)
    {
        toTransport.SetParent(_transporter.transform, true);
    }

    public void StopTransport(TT_Transporter _transporter)
    {
        toTransport.SetParent(defaultParent, true);
    }
}
