﻿using UnityEngine;

public class TT_Transporter : MonoBehaviour
{
    private void OnCollisionEnter(Collision _collision)
    {
        TT_Traveler _traveler = _collision.collider.GetComponent<TT_Traveler>();
        _traveler?.StartTransport(this);
    }

    private void OnCollisionExit(Collision _collision)
    {
        TT_Traveler _traveler = _collision.collider.GetComponent<TT_Traveler>();
        _traveler?.StopTransport(this);
    }
}
