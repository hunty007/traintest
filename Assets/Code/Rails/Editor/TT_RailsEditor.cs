﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CustomEditor(typeof(TT_Rails))]
public class TT_RailsEditor : Editor
{
    TT_Rails rails = null;

    private void OnEnable()
    {
        rails = target as TT_Rails;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Regenerate rails"))
        {
            rails.RegenerateRails();
        }
    }
}
