﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TT_Rails : MonoBehaviour
{
    [SerializeField] TT_Curve path = null;

    [Header("Settings")]
    [SerializeField] LineRenderer rightRailLineRenderer = null;
    [SerializeField] LineRenderer leftRailLineRenderer = null;
    [SerializeField] int railResolution = 50;
    [SerializeField] int railsDistance = 5;

    public void RegenerateRails()
    {
        if (path == null || railResolution < 2)
        {
            return;
        }

        float _halfOfRailDistance = railsDistance / 2f;
        List<Vector3> _rightRailPositions = new List<Vector3>();
        List<Vector3> _leftRailPositions = new List<Vector3>();

        for (int i = 0; i < railResolution; i++)
        {
            float _progress = i / (float)(railResolution - 1);
            Vector3 _position = path.GetPosition(_progress);
            Vector3 _direction = path.GetDirection(_progress);

            Vector3 _rightRailPosition = Vector3.Cross(_direction, Vector3.up).normalized;
            Vector3 _leftRailPosition = -_rightRailPosition;

            _rightRailPositions.Add(_position + _rightRailPosition * _halfOfRailDistance);
            _leftRailPositions.Add(_position + _leftRailPosition * _halfOfRailDistance);
        }

        if (rightRailLineRenderer != null)
        {
            rightRailLineRenderer.positionCount = railResolution;
            rightRailLineRenderer.SetPositions(_rightRailPositions.ToArray());
        }

        if (leftRailLineRenderer != null)
        {
            leftRailLineRenderer.positionCount = railResolution;
            leftRailLineRenderer.SetPositions(_leftRailPositions.ToArray());
        }
    }
}
