﻿using UnityEngine;

public abstract class TT_Curve : MonoBehaviour
{
    public abstract Vector3 GetPosition(float _progress);

    public abstract Vector3 GetDirection(float _progress);
}
