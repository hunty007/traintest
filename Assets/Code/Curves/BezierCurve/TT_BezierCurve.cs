﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TT_BezierCurve : TT_Curve
{
    public List<TT_BezierPoint> BezierPoints = new List<TT_BezierPoint>();

    public override Vector3 GetPosition(float _progress)
    {
        TT_BezierPoint _bezierPoint = getBezierPointTest(ref _progress);
        Vector3 _position = _bezierPoint.GetPosition(_progress);

        return _position;
    }

    public override Vector3 GetDirection(float _progress) // first derivative
    {
        TT_BezierPoint _bezierPoint = getBezierPointTest(ref _progress);

        float _oneMinusProgress = 1 - _progress;
        Vector3 _direction = 3f * Mathf.Pow(_oneMinusProgress, 2f) * (_bezierPoint.StartTangent - _bezierPoint.StartPoint)
            + 6f * _oneMinusProgress * _progress * (_bezierPoint.EndTangent - _bezierPoint.StartTangent)
            + 3f * Mathf.Pow(_progress, 2f) * (_bezierPoint.EndPoint - _bezierPoint.EndTangent);

        _direction.Normalize();
        return _direction;
    }

    public float GetLength()
    {
        float _length = 0f;
        for (int i = 0; i < BezierPoints.Count; i++)
        {
            _length += BezierPoints[i].GetLength();
        }
        return _length;
    }

    private TT_BezierPoint getBezierPoint(ref float _progress)
    {
        int _bezierPointId = 0;
        if (_progress >= 1f)
        {
            _progress = 1f;
            _bezierPointId = BezierPoints.Count - 1;
        }
        else
        {
            _progress = Mathf.Clamp01(_progress) * BezierPoints.Count;
            _bezierPointId = (int)_progress;
            _progress -= _bezierPointId;
        }

        TT_BezierPoint _bezierPoint = BezierPoints[_bezierPointId];
        return _bezierPoint;
    }

    private TT_BezierPoint getBezierPointTest(ref float _progress)
    {
        float _fullLength = GetLength();

        if (_progress >= 1f)
        {
            _progress = 1f;
        }
        float _lengthFromProgress = _progress * _fullLength;

        float _length = 0f;
        int _bezierPointId = 0;
        for (int i = 0; i < BezierPoints.Count - 1; i++)
        {
            if (_length + BezierPoints[i].GetLength() <= _lengthFromProgress)
            {
                _length += BezierPoints[i].GetLength();
                _bezierPointId = i + 1;
            }
            else
            {
                break;
            }
        }

        float _l = _lengthFromProgress - _length;
        _progress = _l / BezierPoints[_bezierPointId].GetLength();
        //Debug.Log($"WTF {_l} :: {_length} :: {_lengthFromProgress}");
        //Debug.Log($"WTF 2 {_progress} :: {_bezierPointId} :: {BezierPoints[_bezierPointId].GetLength()}");

        //Debug.Log(BezierPoints[0].GetLength());

        TT_BezierPoint _bezierPoint = BezierPoints[_bezierPointId];
        return _bezierPoint;
    }
}
