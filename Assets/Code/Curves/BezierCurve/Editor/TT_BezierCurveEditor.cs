﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TT_BezierCurve))]
public class TT_BezierCurveEditor : Editor
{
    TT_BezierCurve bezierCurve = null;
    SerializedProperty bezierPointsProperty = null;

    private void OnEnable()
    {
        bezierCurve = target as TT_BezierCurve;

        bezierPointsProperty = serializedObject.FindProperty("BezierPoints");
    }

    public override void OnInspectorGUI()
    {
        using(new EditorGUILayout.HorizontalScope())
        {
            if (bezierPointsProperty.arraySize > 0)
            {
                bezierPointsProperty.isExpanded = EditorGUILayout.Foldout(bezierPointsProperty.isExpanded,
                    $"{bezierPointsProperty.name}({bezierPointsProperty.arraySize}):", true);
            }
            else
            {
                EditorGUILayout.LabelField("No bezier points.");
            }

            if(GUILayout.Button("Add", GUILayout.Width(100f)))
            {
                bezierPointsProperty.InsertArrayElementAtIndex(bezierPointsProperty.arraySize);
            }
        }

        if (bezierPointsProperty.isExpanded)
        {
            EditorGUI.indentLevel++;

            for (int i = 0; i < bezierPointsProperty.arraySize; i++)
            {
                SerializedProperty _bezierPoint = bezierPointsProperty.GetArrayElementAtIndex(i);
                SerializedProperty _startPoint = _bezierPoint.FindPropertyRelative("StartPoint");
                SerializedProperty _endPoint = _bezierPoint.FindPropertyRelative("EndPoint");
                SerializedProperty _startTangent = _bezierPoint.FindPropertyRelative("StartTangent");
                SerializedProperty _endTangent = _bezierPoint.FindPropertyRelative("EndTangent");

                using (new EditorGUILayout.HorizontalScope())
                {
                    _bezierPoint.isExpanded = EditorGUILayout.Foldout(_bezierPoint.isExpanded, $"Point {i + 1}");
                    if (GUILayout.Button("Remove", GUILayout.Width(100f)))
                    {
                        bezierPointsProperty.DeleteArrayElementAtIndex(i);
                        i--;

                        continue;
                    }
                }

                if (_bezierPoint.isExpanded)
                {
                    EditorGUI.indentLevel++;

                    if (i == 0)
                    {
                        _startPoint.vector3Value = EditorGUILayout.Vector3Field(_startPoint.name, _startPoint.vector3Value);
                    }
                    else
                    {
                        SerializedProperty _previousBezierPoint = bezierPointsProperty.GetArrayElementAtIndex(i - 1);
                        SerializedProperty _previousEndPoint = _previousBezierPoint.FindPropertyRelative("EndPoint");

                        _startPoint.vector3Value = _previousEndPoint.vector3Value;
                        GUI.enabled = false;
                        EditorGUILayout.Vector3Field(_startPoint.name, _startPoint.vector3Value);
                        GUI.enabled = true;
                    }

                    _endPoint.vector3Value = EditorGUILayout.Vector3Field(_endPoint.name, _endPoint.vector3Value);
                    _startTangent.vector3Value = EditorGUILayout.Vector3Field(_startTangent.name, _startTangent.vector3Value);
                    _endTangent.vector3Value = EditorGUILayout.Vector3Field(_endTangent.name, _endTangent.vector3Value);

                    EditorGUI.indentLevel--;
                }
            }

            EditorGUI.indentLevel--;
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        TT_BezierCurve _bezierCurve = target as TT_BezierCurve;
        if (_bezierCurve == null)
        {
            return;
        }

        List<TT_BezierPoint> _bezierPoints = _bezierCurve.BezierPoints;
        if (_bezierPoints == null)
        {
            return;
        }

        for (int i = 0; i < _bezierPoints.Count; i++)
        {
            TT_BezierPoint _bezierPoint = _bezierPoints[i];
            _bezierPoint.StartPoint = Handles.PositionHandle(_bezierPoint.StartPoint, Quaternion.identity);
            _bezierPoint.EndPoint = Handles.PositionHandle(_bezierPoint.EndPoint, Quaternion.identity);
            _bezierPoint.StartTangent = Handles.PositionHandle(_bezierPoint.StartTangent, Quaternion.identity);
            _bezierPoint.EndTangent = Handles.PositionHandle(_bezierPoint.EndTangent, Quaternion.identity);

            Handles.DrawBezier(_bezierPoint.StartPoint, _bezierPoint.EndPoint, _bezierPoint.StartTangent, _bezierPoint.EndTangent, Color.red, null, 2f);
        }
    }
}
