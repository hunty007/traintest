﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TT_BezierPoint
{
    public Vector3 StartPoint = Vector3.zero;
    public Vector3 EndPoint = Vector3.zero;
    [Space]
    public Vector3 StartTangent = Vector3.zero;
    public Vector3 EndTangent = Vector3.zero;

    public Vector3 GetPosition(float _progress)
    {
        float _oneMinusProgress = 1 - _progress;
        Vector3 _position = Mathf.Pow(_oneMinusProgress, 3f) * StartPoint
            + 3 * Mathf.Pow(_oneMinusProgress, 2f) * _progress * StartTangent
            + 3 * _oneMinusProgress * Mathf.Pow(_progress, 2f) * EndTangent
            + Mathf.Pow(_progress, 3f) * EndPoint;

        return _position;
    }

    //public Vector3 GetPosition(float _progress) // De Casteljau's Algorithm
    //{
    //    float _oneMinusProgress = 1 - _progress;

    //    Vector3 _q = Vector3.Lerp(StartPoint, StartTangent, _progress);
    //    Vector3 _r = Vector3.Lerp(StartTangent, EndTangent, _progress);
    //    Vector3 _s = Vector3.Lerp(EndTangent, EndPoint, _progress);

    //    Vector3 _p = Vector3.Lerp(_q, _r, _progress);
    //    Vector3 _t = Vector3.Lerp(_r, _s, _progress);

    //    Vector3 _position = Vector3.Lerp(_p, _t, _progress);
    //    return _position;
    //}

    public float GetLength(int _resolution = 10)
    {
        List<Vector3> _positions = new List<Vector3>();
        for (int i = 0; i < _resolution; i++)
        {
            float _progress = i / (float)(_resolution - 1);
            Vector3 _position = GetPosition(_progress);
            _positions.Add(_position);
        }

        float _length = 0f;
        for (int i = 0; i < _positions.Count - 1; i++)
        {
            _length += Vector3.Distance(_positions[i], _positions[i + 1]);
        }
        return _length;
    }
}
